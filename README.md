# Itrellis Demo

## Author 
This is programmed by Indraneel Dey ( indraneel.dey@gmail.com ) as a demo of his skills. The software uses Docker, NetCore (C#) for middleware, and React for frontend.

## How to run
* Need to have Docker installed on your system, and set for Linux containers.
* In a terminal go to the location of the docker-compose.yml file and type in:
    * > *docker-compose up -d*
* Go to your browser and go to http://localhost
* To rebuild follow these steps:
    * In terminal: 
        > docker-compose down
        
        > docker system prune
        
        > dock-compose up --build -d
