namespace middleware.Models
{
    /// <summary>
    /// Return model for Makes
    /// </summary>
    public class MakesJson
    {
        public int CarMakeId { get; set; }
        public string Make { get; set; }
    }
}