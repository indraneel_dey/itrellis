namespace middleware.Models
{
    /// <summary>
    /// Return model for colors
    /// </summary>
    public class ColorsJson
    {
        public int CarColorId { get; set; }
        public string Color { get; set; }
    }
}