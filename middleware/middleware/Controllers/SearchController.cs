using System;
using Microsoft.AspNetCore.Mvc;
using Middleware.Db;
using Middleware.Db.Interface;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace middleware.Controllers
{
    [Route("api/[controller]")]
    public class SearchController : Controller
    {
        /// <summary>
        /// Main search method
        /// </summary>
        /// <param name="search"></param>
        /// <returns>Listing of <see cref="CarModel"/></returns>
        [Route("")]
        [HttpPost]
        public JArray Search([FromBody] SearchModel search)
        {
            try
            {
                var results = CarModelInterface.Search(search);
                
                return JArray.FromObject(results);
 
            }
            catch (Exception e)
            {
               return new JArray();//JObject.FromObject(e);
            }
        }

        /// <summary>
        /// Makes sure that GET endpoint is taken care of
        /// </summary>
        /// <returns></returns>
        [Route("")]
        [HttpGet]
        public string SearchGet()
        {
            return "Nothing here but us chickens";
        }
    }
}