using System.Collections.Generic;
using System.Runtime.InteropServices;
using AutoMapper;
using middleware.Models;
using Microsoft.AspNetCore.Mvc;
using Middleware.Db;
using Middleware.Db.Interface;
using Newtonsoft.Json.Linq;

namespace middleware.Controllers
{
    [Route("api/[controller]")]
    public class GetController
    {
        /// <summary>
        /// Get all colors
        /// </summary>
        /// <returns>Listing of all colors in JSON</returns>
        [HttpGet]
        [Route("colors")]
        public JArray GetAllColors()
        {
            
            return JArray.FromObject(CarColorInterface.GetAll());
        }

        /// <summary>
        /// Get all makes of cars
        /// </summary>
        /// <returns>Returns all makes of cars in JSON</returns>
        [HttpGet]
        [Route("makes")]
        public JArray GetAllMakes()
        {
           
            return JArray.FromObject(CarMakeInterface.GetAll());
        }
    }
}