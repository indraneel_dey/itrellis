using System;
using Middleware.Db;
using Middleware.Db.Interface;
using NUnit.Framework;

namespace Middleware.Unit
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void SearchingColor()
        {
            var search = new SearchModel
            {
                Colors = new int[]{1}
            };

            var results = CarModelInterface.Search(search);
            
            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(results);
                Assert.IsTrue(results.Count>1);
            });
        }
        
        [Test]
        public void SearchingPrice()
        {
            var search = new SearchModel
            {
                Price = 15000
            };

            var results = CarModelInterface.Search(search);
            
            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(results);
                Assert.IsTrue(results.Count>1);
            });
        }
        
        [Test]
        public void SearchingOption()
        {
            var search = new SearchModel
            {
                HeadedSeats = true
            };

            var results = CarModelInterface.Search(search);
            
            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(results);
                Assert.IsTrue(results.Count>1);
            });
        }
    }
}