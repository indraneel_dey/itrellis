namespace Middleware.Db
{
    /// <summary>
    /// Dataset Model
    /// </summary>
    public class DatasetJson
    {
        public string _id { get; set; }
        public string make { get; set; }
        public int year { get; set; }
        public string color { get; set; }
        public long price { get; set; }
        public bool hasSunroof { get; set; }
        public bool isFourWheelDrive { get; set; }
        public bool hasLowMiles { get; set; }
        public bool hasPowerWindows { get; set; }
        public bool hasNavigation { get; set; }
        public bool hasHeatedSeats { get; set; }
    }
}