namespace Middleware.Db
{
    /// <summary>
    /// Search Model
    /// </summary>
    public class SearchModel
    {
        public int[] Colors { get; set; }
        public int[] Makes { get; set; }
        public int Price { get; set; }
        public bool Sunroof { get; set; }
        public bool FourWheelDrive { get; set; }
        public bool LowMiles { get; set; }
        public bool PowerWindows { get; set; }
        public bool Navigation { get; set; }
        public bool HeadedSeats { get; set; }
    }
}