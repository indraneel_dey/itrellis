using System.Collections;
using System.Collections.Generic;

namespace Middleware.Db
{
    /// <summary>
    /// Car Color Model
    /// </summary>
    public class CarColor
    {
        public int CarColorId { get; set; }
        public string Color { get; set; }
        public ICollection<CarModel> Models { get; set; }
    }
}