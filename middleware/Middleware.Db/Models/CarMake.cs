using System.Collections;
using System.Collections.Generic;

namespace Middleware.Db
{
    /// <summary>
    /// Car Make Model
    /// </summary>
    public class CarMake
    {
        public int CarMakeId { get; set; }
        public string Make { get; set; }
        public ICollection<CarModel> Models { get; set; }
    }
}