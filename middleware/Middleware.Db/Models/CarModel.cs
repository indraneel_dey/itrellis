﻿using System;

namespace Middleware.Db
{
    /// <summary>
    /// Main Car Model
    /// </summary>
    public class CarModel
    {
        public int CarModelId { get; set; }
        public string CarId { get; set; }
        public int MakeId { get; set; }
        public CarMake Make { get; set; }
        public int Year { get; set; }
        
        public int ColorId { get; set; }
        public CarColor Color { get; set; }

        public long Price { get; set; } = 0;
        public bool HasSunroof { get; set; } = false;
        public bool IsFourWheelDrive { get; set; } = false;
        public bool HasLowMiles { get; set; } = false;
        public bool HasPowerWindows { get; set; } = false;
        public bool HasNavigation { get; set; } = false;
        public bool HasHeatedSeats { get; set; } = false;
    }
}