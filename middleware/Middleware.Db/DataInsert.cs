using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using AutoMapper;
using Dapper;
using Middleware.Db.Context;
using Middleware.Db.Interface;
using Middleware.Db.Mapper;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Middleware.Db
{
    /// <summary>
    /// Inserts data
    /// </summary>
    public class DataInsert
    {
        static DataInsert()
        {
            using (var context = new CarContext())
            {
                //Makes sure data doesnt exist for demo
                using (DbConnection db = new MySqlConnection(Helper.CONN))
                {
                    var query = $"DROP TABLE IF EXISTS {nameof(CarModel)};" +
                                $"DROP TABLE IF EXISTS {nameof(CarColor)};" +
                                $"DROP TABLE IF EXISTS {nameof(CarMake)};";
                    try
                    {
                        db.Execute(query);
                    }
                    catch (DbException)
                    {
                    }

                }


                // Creates the database if not exists
                context.Database.EnsureCreated();

                
                List<DatasetJson> items = new List<DatasetJson>();
                List<CarColor> colors = new List<CarColor>();
                List<CarMake> makes = new List<CarMake>();
                
                //Reading data and pushing into variable
                using (StreamReader r = new StreamReader("dataset.json"))
                {
                    string json = r.ReadToEnd();
                    items = JsonConvert.DeserializeObject<List<DatasetJson>>(json);
                }
                
                //Gets all the makes and colors from the dataset
                items.ForEach(o =>
                {
                    if (!colors.Exists(x=>x.Color==o.color)) colors.Add(new CarColor{Color = o.color});
                    if (!makes.Exists(x=>x.Make==o.make)) makes.Add(new CarMake{Make = o.make});
                });

                context.CarColor.AddRange(colors);
                context.CarMake.AddRange(makes);
                context.SaveChanges();
                
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<JsonToDbProfile>();
                });

                var mapper = config.CreateMapper();
                var list = mapper.Map<List<CarModel>>(items);
                
                //Inserts data
                CarModelInterface.Insert(list);
                //context.SaveChanges();
                
            }
        }
    }
}