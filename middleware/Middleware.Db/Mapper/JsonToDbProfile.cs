using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Middleware.Db.Interface;

namespace Middleware.Db.Mapper
{
    public class JsonToDbProfile : Profile
    {
        /// <summary>
        /// Mapper from JSON dataset to DB
        /// </summary>
        public JsonToDbProfile()
        {
            List<CarColor> colors = CarColorInterface.GetAll();
            List<CarMake> makes = CarMakeInterface.GetAll();

            CreateMap<DatasetJson, CarModel>()
                .ForMember(d => d.CarId, opt => opt.MapFrom(d => d._id))
                .ForMember(d => d.MakeId,
                    opt => opt.MapFrom(d => makes.SingleOrDefault(o => o.Make == d.make).CarMakeId))
                .ForMember(d => d.ColorId, 
                    opt => opt.MapFrom(d => colors.SingleOrDefault(o => o.Color == d.color).CarColorId))
                .ForMember(d => d.Price, opt => opt.MapFrom(d => d.price))
                .ForMember(d => d.Year, opt => opt.MapFrom(d => d.year))
                .ForMember(d => d.HasNavigation, opt => opt.MapFrom(d => d.hasNavigation))
                .ForMember(d => d.HasSunroof, opt => opt.MapFrom(d => d.hasSunroof))
                .ForMember(d => d.IsFourWheelDrive, opt => opt.MapFrom(d => d.isFourWheelDrive))
                .ForMember(d => d.HasLowMiles, opt => opt.MapFrom(d => d.hasLowMiles))
                .ForMember(d => d.HasPowerWindows, opt => opt.MapFrom(d => d.hasPowerWindows))
                .ForMember(d => d.HasNavigation, opt => opt.MapFrom(d => d.hasNavigation))
                .ForMember(d => d.HasHeatedSeats, opt => opt.MapFrom(d => d.hasHeatedSeats))
                .ForMember(d=>d.CarModelId,opt=>opt.AllowNull())
                .ForAllOtherMembers(o=>o.Ignore())
                ;
        }
    }
}