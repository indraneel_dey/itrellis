using System;
using System.Net;
using System.Net.Sockets;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Middleware.Db.Context
{
    
    /// <summary>
    /// Car Context
    /// </summary>
    public class CarContext: DbContext
    {
        public DbSet<CarModel> CarModel { get; set; }
        public DbSet<CarColor> CarColor { get; set; }
        public DbSet<CarMake> CarMake { get; set; }
        
        /// <summary>
        /// On Configuration
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {       
            optionsBuilder.UseMySQL(Helper.CONN);
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CarModel>(builder =>
            {
                
                //builder.HasOne(e => e.Make).WithMany(f => f.Models).HasForeignKey(k=>k.MakeId);
                //builder.HasOne(e => e.Color).WithMany(f=>f.Models).HasForeignKey(k=>k.ColorId);
                //builder.HasKey(e => e.CarModelId);
            });
        }
        
    }
}