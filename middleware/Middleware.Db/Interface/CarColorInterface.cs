using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Dapper;
using Microsoft.EntityFrameworkCore.Update;
using MySql.Data.MySqlClient;

namespace Middleware.Db.Interface
{
    public static class CarColorInterface
    {
        /// <summary>
        /// Gets all colors for cars
        /// </summary>
        /// <returns>List of <see cref="CarColor"/></returns>
        public static List<CarColor> GetAll()
        {
            try
            {
                var builder = new SqlBuilder();
                var query = builder.AddTemplate($"SELECT * FROM {nameof(CarColor)}");

                var results = new List<CarColor>();
                
                using (DbConnection db = new MySqlConnection(Helper.CONN))
                {
                   results = db.Query<CarColor>(query.RawSql).ToList();
                }

                return results;
            }
            catch (Exception)
            {
                return new List<CarColor>();
            }
        }
    }
}