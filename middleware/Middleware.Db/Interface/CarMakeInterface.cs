using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Dapper;
using MySql.Data.MySqlClient;

namespace Middleware.Db.Interface
{
    public class CarMakeInterface
    {
        /// <summary>
        /// Gets all makes of cars
        /// </summary>
        /// <returns>List of <see cref="CarMake"/></returns>
        public static List<CarMake> GetAll()
        {
            try
            {
                var builder = new SqlBuilder();
                var query = builder.AddTemplate($"SELECT * FROM {nameof(CarMake)}");

                var results = new List<CarMake>();
                
                using (DbConnection db = new MySqlConnection(Helper.CONN))
                {
                    results = db.Query<CarMake>(query.RawSql).ToList();
                }

                return results;
            }
            catch (Exception)
            {
                return new List<CarMake>();
            }
        }
    }
}