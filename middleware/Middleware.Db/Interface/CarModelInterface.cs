using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection.Metadata.Ecma335;
using Dapper;
using MySql.Data.MySqlClient;

namespace Middleware.Db.Interface
{
    public static class CarModelInterface
    {
        public static bool Insert(CarModel model)
        {
            try
            {
                var builder = new SqlBuilder();
                var queryString =
                    $"INSERT INTO {nameof(CarModel)} (CarModelId, CarId, MakeId, Year , ColorId, Price, HasSunroof, " +
                    $"IsFourWheelDrive, HasLowMiles, HasPowerWindows, HasNavigation, HasHeatedSeats)" +
                    $"VALUES (@CarModelId, @CarId, @MakeId, @Year , @ColorId, @Price,@HasSunroof, @IsFourWheelDrive, @HasLowMiles,@HasPowerWindows, @HasNavigation, @HasHeatedSeats)";
                var query = builder.AddTemplate(queryString);

                int results = 0;
                using (DbConnection db = new MySqlConnection(Helper.CONN))
                {
                   results = db.Execute(query.RawSql, model);
                }

                return (results > 0);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool Insert(List<CarModel> models)
        {
            try
            {
                models.ForEach(o => Insert(o));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Searches through database for cars
        /// </summary>
        /// <param name="search">Data taken in from API call</param>
        /// <returns>List of <see cref="CarModel"/></returns>
        public static List<CarModel> Search(SearchModel search)
        {
            try
            {
                var builder = new SqlBuilder();
                var query = builder.AddTemplate($"SELECT * FROM {nameof(CarModel)} /**innerjoin**/ /**where**/");
                
                if (search.Colors?.Length >= 1)
                {
                    builder.Where($"{nameof(CarModel.ColorId)} in ({string.Join(",", search.Colors)})");
                }

                if (search.Makes?.Length >= 1)
                {
                    builder.Where($"{nameof(CarModel.MakeId)} in ({string.Join(",", search.Makes)})");
                }

                
                if (search.Price > 0)
                {
                    builder.Where($"{nameof(CarModel.Price)} <= {search.Price}");
                }

                if (search.Sunroof)
                {
                    builder.Where($"{nameof(CarModel.HasSunroof)}={search.Sunroof}");
                }
                
                if (search.Navigation)
                {
                    builder.Where($"{nameof(CarModel.HasNavigation)}={search.Navigation}");
                }
                
                if (search.LowMiles)
                {
                    builder.Where($"{nameof(CarModel.HasLowMiles)}={search.LowMiles}");
                }
                
                if (search.HeadedSeats)
                {
                    builder.Where($"{nameof(CarModel.HasHeatedSeats)}={search.HeadedSeats}");
                }
                
                if (search.PowerWindows)
                {
                    builder.Where($"{nameof(CarModel.HasPowerWindows)}={search.PowerWindows}");
                }

                if (search.FourWheelDrive)
                {
                    builder.Where($"{nameof(CarModel.IsFourWheelDrive)}={search.FourWheelDrive}");
                }

                
                //Ensures that Color and Make objects are populated and pulled through
                builder.InnerJoin($"{nameof(CarMake)} on {nameof(CarModel)}.{nameof(CarModel.MakeId)} = {nameof(CarMake)}.{nameof(CarMake.CarMakeId)}");
                builder.InnerJoin(
                    $"{nameof(CarColor)} on {nameof(CarModel)}.{nameof(CarModel.ColorId)} ={nameof(CarColor)}.{nameof(CarColor.CarColorId)}");
                
                var results = new List<CarModel>();
                using (DbConnection db = new MySqlConnection(Helper.CONN))
                {
                    results = db.Query<CarModel,CarMake,CarColor,CarModel>(query.RawSql, (car,make, color) =>
                    {
                        car.Color = color;
                        car.Make = make;
                        return car;
                    },splitOn:"CarMakeId,CarColorId").ToList();
                }

                return results;

            }
            catch (Exception)
            {
                return new List<CarModel>();
            }
        }
    }
}