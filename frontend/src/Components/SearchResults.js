import React from 'react';
import {CarTile} from "./SearchResult/CarTile";

export class SearchResults extends React.Component{
    render() {
        console.log(this.props.results)
        return (
                <section>
                    {
                        this.props.results.map(e=><CarTile car={e}/>)
                    }
                </section>
        );
    }
}