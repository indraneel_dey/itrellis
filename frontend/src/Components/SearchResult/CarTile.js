import React from 'react';

export class CarTile extends React.Component{
    render() {
        let car = this.props.car;
        let options = "";
        options += car.HasSunroof  ? "Sunroof, ": "";
        options += car.IsFourWheelDrive  ? "Four Wheel Drive, ": "";
        options += car.HasLowMiles  ? "Low Miles, ": "";
        options += car.HasPowerWindows  ? "Power Windows, ": "";
        options += car.HasNavigation  ? "Navigation, ": "";
        options += car.HeadedSeats  ? "Headed Seats, ": "";

       return(
           <article className={"container"} style={{padding:"5px", paddingBottom: "10px", borderBottom: "1px solid black"}}>
               <section className={"row"}>
                   <div class="col">
                       <div>Price: {car.Price}</div>
                       <div>Id: {car.CarId}</div>
                       <div>Make: {car.Make.Make}</div>
                       <div>Color: {car.Color.Color}</div>
                   </div>
                   <div class="col">
                       <div>{options.slice(0,options.length-2)}</div>
                   </div>
               </section>
           </article>


       );
    }
}