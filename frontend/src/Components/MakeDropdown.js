import React, {Component} from 'react';
import App from "../App";

export class MakeDropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            makeOptions: [],
            make: []
        }
        //this.handleChange = this.handleChange.bind(this);
    }

    handleChange = e => {
        const {name,options} = e.target;
        //let options = e.target.options;
        var value = [];

        for (var i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                value.push(options[i].value);
            }
        }

        this.setState({
            [e.target.name]: value
        },()=>{
            this.props.onMakeChange(this.state);
        });

    }

    componentDidMount() {
        let makeInit = [];
        fetch("http://localhost:8080/api/get/makes").then(response => {
            return response.json();
        }).then(data => {
            makeInit.push(data);
        }).then(() => {
            this.setState({
                makeOptions: makeInit
            });
        });
    }

    render() {
        return (
            <div>
                <select multiple className="form-control" name={"make"}
                        value={this.state.make.value} onChange={this.handleChange}>
                    {this.state.makeOptions.map(el =>
                        el.map(e => <option value={e.CarMakeId}>{e.Make}</option>
                        )
                    )}
                </select>
            </div>

        )
    }
}