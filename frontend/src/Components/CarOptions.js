import React from 'react';

export class CarOptions extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            Options:[
                {name:"sunroof", label:"Has SunRoof", type:"checkbox"},
                {name:"lowmiles", label: "Low Miles", type:"checkbox"},
                {name:"powerwindows", label: "Power Windows", type:"checkbox"},
                {name:"navigation", label:"Navigation", type:"checkbox"},
                {name:"heatedseats",label:"Heated Seats", type:"checkbox"},
                {name:"fourwheeldrive",label:"Four Wheel Drive", type:"checkbox"}
            ],
            sunroof: false,
            lowmiles: false,
            powerwindows: false,
            navigation: false,
            heatedseats: false,
            fourwheeldrive: false
        }

        this.handelSelect = this.handelSelect.bind(this);
    }

    handelSelect = e => {
        const {name, value} = e.target;
        this.setState({
            [name]: e.target.checked
        },()=>{
            this.props.onOptionsChange(this.state);
        })

    }

    render() {
        return (
            this.state.Options.map(el=>
                <div className="input-group">
                    <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <div className="input-group-text">
                                <input type={el.type} name={el.name} onChange={this.handelSelect}
                                       aria-label="Checkbox for following text input"/>
                                <span>&nbsp;{el.label}</span>
                            </div>
                        </div>
                    </div>
                </div>
            )
        );
    }
}