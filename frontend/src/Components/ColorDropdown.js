import React from 'react';

export class ColorDropdown extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            colorOptions: [],
            color: []
        }
    }

    handleChange = e => {
        const {name,options} = e.target;
        //let options = e.target.options;
        var value = [];

        for (var i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                value.push(options[i].value);
            }
        }

        this.setState({
            [e.target.name]: value
        },()=>{
            this.props.onColorChange(this.state);
        });

    }

    componentDidMount() {
        let colorsInit = [];
        fetch("http://localhost:8080/api/get/colors").then(response => {
            return response.json();
        }).then(data => {
            colorsInit.push(data);
        }).then(() => {
            this.setState({
                colorOptions: colorsInit
            });
        });
    }

    render() {

        return (
            <div>
                <select multiple className="form-control" name={"color"}
                        value={this.state.color.value} onChange={this.handleChange}>
                    {this.state.colorOptions.map(el =>
                        el.map(e => <option value={e.CarColorId}>{e.Color}</option>
                        )
                    )}
                </select>
                {this.state.colorOptions.map(el =>
                    <div>{el.Color}</div>
                )}
            </div>
        )
    }
}
