import React, {Component} from 'react';
import './App.css';
import {Form, Label, Input} from 'reactstrap'

import {ColorDropdown} from "./Components/ColorDropdown"
import {MakeDropdown} from "./Components/MakeDropdown"
import {CarOptions} from "./Components/CarOptions"
import {SearchResults} from "./Components/SearchResults";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            makeDropdown: [],
            colorDropdown: [],
            options: [],
            price: "",
            results: []
        }
    }

    handelChange = e => {
        const {name, value} = e.target;
        this.setState(() => ({
            [name]: value
        }))
    }

    handleSubmit = e => {
        e.preventDefault();
        let options = this.state.options;
        let query = {
            Colors: this.state.colorDropdown.color ? this.state.colorDropdown.color : [],
            Makes: this.state.makeDropdown.make ? this.state.makeDropdown.make : [],
            Price: this.state.price? this.state.price : 0,
            Sunroof: options.sunroof ? options.sunroof : false,
            LowMiles: options.lowmiles ? options.lowmiles : false,
            PowerWindows: options.powerwindows ? options.powerwindows : false,
            Navigation: options.navigation ? options.navigation : false,
            HeatedSeats: options.heatedseats ? options.heatedseats : false,
            FourWheelDrive: options.fourwheeldrive ? options.fourwheeldrive : false

        }

        fetch("http://localhost:8080/api/search",{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(query)
        }).then(response=>{
            return response.json()
        }).then(data =>{

            this.setState({results: data});
        });
    }

    handelMakeChange = data =>{
        //console.log(data)
        this.setState({makeDropdown:data},()=>{
            console.log(this.state)
        });

    }

    handelColorChange = data =>{
        //console.log(data)
        this.setState({colorDropdown:data},()=>{
            console.log(this.state)
        })
    }

    handelOptionsChange = data =>{
        //console.log(data)
        this.setState({options:data},()=>{
            console.log(this.state);
        })
    }

    render() {

        return (
            <Form onSubmit={this.handleSubmit} className={"col"}>
                <article class="row">
                    <secton class="col-4">
                        <div className="input-group">
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <div className="input-group-text">
                                        <span>Make &nbsp;</span>
                                        <MakeDropdown data={this.state.makeDropdown} onMakeChange={this.handelMakeChange}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="input-group">
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <div className="input-group-text">
                                        <span>Color&nbsp;</span>
                                        <ColorDropdown data={this.state.colorDropdown} onColorChange={this.handelColorChange}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="input-group">
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <div className="input-group-text">
                                        <span>Price&nbsp;</span>
                                        <input type={"number"} name={"price"} onChange={this.handelChange}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <CarOptions data={this.state.options} onOptionsChange={this.handelOptionsChange}/>
                        <input type={"submit"} className="btn"/>
                    </secton>
                    <section className={"col"}>
                        <SearchResults results={this.state.results}/>
                    </section>
                </article>

            </Form>


        );
    }
}

export default App;
